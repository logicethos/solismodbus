﻿using System.Net;
using SolisModbus;
using SolisModbusTest;
using Spectre.Console;


public static class Program
{
    static void Main()
    {
        byte slaveId = 1;

        //using (var modbus = new ModbusClient(IPEndPoint.Parse("192.168.0.143:8899")))
        using (var modbus = new ModbusClient("/tmp/virtualSerialPort0",38400))
        {

    //        Console.WriteLine(modbus.GetRegister(33251));

            AnsiConsole.Write(modbus.GetInverter(slaveId).ToProtoTree());
            AnsiConsole.Write(modbus.GetInverterStatus(slaveId).ToProtoTree());

            AnsiConsole.Write(modbus.GetDcPower(slaveId).ToProtoTree());
            AnsiConsole.Write(modbus.GetAcPower(slaveId).ToProtoTree());

            AnsiConsole.Write(modbus.GetTotalGeneration(slaveId).ToProtoTree());
            AnsiConsole.Write(modbus.GetMeter(slaveId).ToProtoTree());

            AnsiConsole.Write(modbus.GetBattery(slaveId).ToProtoTree());

        }

        using (var meter = new MeterListener("/tmp/virtualSerialPort1"))
        {
            meter.Start();
            meter.NewReading += (source, args) =>
            {
                AnsiConsole.Write(meter.MeterLive.ToProtoTree());
            };
            Thread.Sleep(5000);
        }

    }
}