﻿using System;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices.JavaScript;
using System.Text;
using NModbus.Device;
using NModbus.Serial;
using NModbus;
using SolisModbus.Proto;
using Google.Protobuf.WellKnownTypes;
using Enum = Google.Protobuf.WellKnownTypes.Enum;

public class ModbusClient : IDisposable
{
    private TcpClient tcpclient;
    private SerialPort port;
    private IModbusMaster master;
    private readonly object _lock = new object();
    private long lastTX =0;
    private readonly int msBetweenReads = 300;
    private IPEndPoint IPaddress;
    private string SerialDevice;
    private int Baud = 9600;

    public ModbusClient(IPEndPoint address)
    {
        IPaddress = address;
        OpenTCP();
    }


    public ModbusClient(string portPath = "/dev/ttyAMA0", int baud = 9600)
    {
        SerialDevice = portPath;
        Baud = baud;
        OpenSerial();
    }

    void OpenTCP()
    {
        tcpclient = new TcpClient();
        Console.WriteLine($"Opening {IPaddress.Address}:{IPaddress.Port}");
        tcpclient.Connect(IPaddress);

        // Thread.Sleep(1000);
        if (tcpclient.Connected)
        {
            var factory = new ModbusFactory();

            master = factory.CreateMaster(tcpclient);
            master.Transport.ReadTimeout = 3000;
            master.Transport.WriteTimeout = 3000;
            master.Transport.Retries = 3;
            master.Transport.WaitToRetryMilliseconds = msBetweenReads;
        }
        else
        {
            Console.WriteLine("Failed to connect");
        }
    }

    void OpenSerial()
    {
        // Set up the serial port configuration.
        SerialPort port = new SerialPort(SerialDevice)
        {
            BaudRate = Baud,
            DataBits = 8,
            Parity = Parity.None,
            StopBits = StopBits.One,
            ReadTimeout = 1000
        };

        Console.WriteLine($"Opening port {port.PortName} Baud:{port.BaudRate}, Timeout {port.ReadTimeout}");
        port.Open();

        if (port.IsOpen)
        {
            // Create the Modbus RTU master.
            var factory = new ModbusFactory();
            master = factory.CreateRtuMaster(port);
            master.Transport.ReadTimeout = 1000;
            master.Transport.WriteTimeout = 1000;
            master.Transport.Retries = 3;
            master.Transport.WaitToRetryMilliseconds = msBetweenReads;
        }
        else
        {
            Console.WriteLine("Failed to open port!");
        }
    }

    public ushort[] ReadInputRegisters(byte slaveId, ushort startAddress, ushort endAddress)
    {
        lock (_lock)
        {
            ushort points = (ushort)(endAddress - startAddress + 1);

            try
            {
                long lastTXDiff = Environment.TickCount64 - lastTX;
                if (lastTXDiff < msBetweenReads) Thread.Sleep((int)(msBetweenReads - lastTXDiff));

                return master.ReadInputRegisters(slaveId, startAddress, points);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error {ex.Message}");
                return null;
            }
            finally
            {
                lastTX = Environment.TickCount64;
            }
        }
    }

    public ushort[] ReadHoldingRegisters(byte slaveId, ushort startAddress, ushort endAddress)
    {
        lock (_lock)
        {
            ushort points = (ushort)(endAddress - startAddress + 1);

            try
            {
                long lastTXDiff = Environment.TickCount64 - lastTX;
                if (lastTXDiff < msBetweenReads) Thread.Sleep((int)(msBetweenReads - lastTXDiff));

                return master.ReadHoldingRegisters(slaveId, startAddress, points);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error {ex.Message}");
                return null;
            }
            finally
            {
                lastTX = Environment.TickCount64;
            }
        }
    }
    

    public ushort ReadInputRegister(byte slaveId, ushort address)
    {
        return ReadInputRegisters(slaveId, address, address)[0];
    }

    public ushort ReadHoldingRegister(byte slaveId, ushort address)
    {
        return ReadHoldingRegisters(slaveId, address, address)[0];
    }

    public void WriteRegister(byte slaveId, ushort address, ushort regValue)
    {
        master.WriteSingleRegister(slaveId,address, regValue);
    }

    

    public Inverter GetInverter(byte slaveId)
    {

        int pointer = 0;
        var registers0 = ReadInputRegisters(slaveId, 33000, 33027);
        var registers1 = ReadInputRegisters(slaveId, 33047, 33048);

        if (registers0 == null || registers1 == null) return null;

        var inverter = new Inverter
        {
            SolisInverterModel = registers0[pointer++].ToString("X"),
            ModelNo = registers0[pointer++].ToString("X"),
            DSPversion = registers0[pointer++].ToString("X"),
            HMIversion = registers0[pointer++].ToString("X"),
            SerialNnumber = GetString(ref registers0, ref pointer, 33019 - 33004 + 1),
            InitialStartupSettingFlagbit = registers0[pointer++] > 0,
        };
        pointer++; //reserved

        var year = registers0[pointer++];
        var month = registers0[pointer++];
        var day = registers0[pointer++];
        var hour = registers0[pointer++];
        var minute = registers0[pointer++];
        var second = registers0[pointer];

        if (month > 0 && month <= 12 && day > 0 && day <= 31)
        {
            if (year < 2000) year += 2000; //Watch out for year 2100 bug!
            inverter.Datetime = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc).ToTimestamp();
        }
        else
        {
            Console.WriteLine($"Invalid date {year} {month} {day} {hour} {minute} {second}");
        }

        pointer = 0;
        inverter.ACoutputType = GetEnum<Inverter.Types.actype>(ref registers1, ref pointer);
        inverter.DCInputCount = registers1[1] + (uint)1;

        return inverter;
    }

    public InverterStatus GetInverterStatus(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadInputRegisters(slaveId, 33091, 33096);
        if (registers == null) return null;

        var inverterStatus = new InverterStatus
        {
            StandardWorkingModes = GetEnum<InverterStatus.Types.workingMode>(ref registers, ref pointer),
            GridStandards = GetUint16(ref registers, ref pointer),
            InverterTemperature = GetInt16(ref registers, ref pointer) * 0.1f,
            GridFrequency = GetUint16(ref registers, ref pointer) * 0.01f,
            InverterCurrentStatus = GetUint16(ref registers, ref pointer),
            LeadAcidBatteryTemperature = GetInt16(ref registers, ref pointer) * 0.1f,
        };
        return inverterStatus;
    }


    public DCPower GetDcPower(byte slaveId)
    {
        int pointer0 = 0;
        int pointer1 = 0;

        var registers0 = ReadInputRegisters(slaveId, 33049, 33058);
        var registers1 = ReadInputRegisters(slaveId, 33071, 33072);

        if (registers0 == null || registers1 == null) return null;


        var dcPower = new DCPower
        {
            Voltage1 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Current1 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Voltage2 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Current2 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Voltage3 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Current3 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Voltage4 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            Current4 = GetUint16(ref registers0, ref pointer0) * 0.1f,
            TotalOutputPower = GetUint32(ref registers0,ref pointer0),
            BusVoltage = GetUint16(ref registers1, ref pointer1) * 0.1f,
            BusHalfVoltage = GetUint16(ref registers1, ref pointer1) * 0.1f,
        };
        return dcPower;
    }

    public ACPower GetAcPower(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadInputRegisters(slaveId, 33073, 33084);
        if (registers == null) return null;

        var acPower = new ACPower
        {
            AVoltage = GetUint16(ref registers, ref pointer) * 0.1f,
            BVoltage = GetUint16(ref registers, ref pointer) * 0.1f,
            CVoltage = GetUint16(ref registers, ref pointer) * 0.1f,
            ACurrent = GetUint16(ref registers, ref pointer) * 0.1f,
            BCurrent = GetUint16(ref registers, ref pointer) * 0.1f,
            CCurrent = GetUint16(ref registers, ref pointer) * 0.1f,
            ActivePower = GetUint32(ref registers, ref pointer),
            ReactivePower = GetInt32(ref registers, ref pointer),
            ApparentPower = GetInt32(ref registers, ref pointer),
        };
        return acPower;
    }

    public TotalGeneration GetTotalGeneration(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadInputRegisters(slaveId, 33029, 33040);
        if (registers == null) return null;

        var totals = new TotalGeneration
        {
            TotalEnergyGeneration = GetUint32(ref registers,ref pointer),
            CurrentMonthEnergyGeneration = GetUint32(ref registers,ref pointer),
            LastMonthEnergyGeneration = GetUint32(ref registers,ref pointer),
            TodayEnergyGeneration = GetUint16(ref registers,ref pointer) * 0.1,
            YesterdayEnergyGeneration = GetUint16(ref registers,ref pointer) * 0.1,
            ThisYearEnergyGeneration = GetUint32(ref registers,ref pointer),
            LastYearEnergyGeneration = GetUint32(ref registers,ref pointer),
        };

        return totals;
    }


    public Battery GetBattery(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadInputRegisters(slaveId, 33132, 33146);
        if (registers == null) return null;

        pointer = 0;
        var battery = new Battery
        {
            StorageControlSwitchingValue = GetEnum<Battery.Types.StorageControlSwitching>(ref registers,ref pointer),
            BatteryVoltage = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryCurrent = GetInt16(ref registers,ref pointer) * 0.1f,
            BatteryCurrentDischarge = GetUint16(ref registers,ref pointer) > 0,
            LLCbusVoltage = GetUint16(ref registers,ref pointer) * 0.1f,
            BackupACVoltagePhaseA = GetUint16(ref registers,ref pointer) * 0.1f,
            BackupACCurrent = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryCapacitySOC = GetUint16(ref registers,ref pointer),
            BatteryHealthSOH = GetUint16(ref registers,ref pointer),
            BatteryVoltageBMS = GetUint16(ref registers,ref pointer) * 0.01f,
            BatteryCurrentBMS = GetInt16(ref registers,ref pointer) * 0.1f,
            BatteryChargeCurrentLimitation = GetUint16(ref registers,ref pointer),
            BatteryDischargeCurrentLimitation = GetUint16(ref registers,ref pointer),
            BatteryFaultStatus01 = GetUint16(ref registers,ref pointer),
            BatteryFaultStatus02 =GetUint16(ref registers,ref pointer),
        };

        if (!battery.BatteryCurrentDischarge && battery.BatteryCurrentBMS > 0) battery.BatteryCurrentBMS = -battery.BatteryCurrentBMS;

        return battery;
    }

    public Meter GetMeter(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadInputRegisters(slaveId, 33250, 33286);
        if (registers == null) return null;

        var meter = new Meter
        {
            MeterCTPosition = GetUint16(ref registers,ref pointer),
            MeterAcVoltageA = GetUint16(ref registers,ref pointer) * 0.1f,
            MeterAcCurrentA = GetUint16(ref registers,ref pointer) * 0.01f,
            MeterAcVoltageB = GetUint16(ref registers,ref pointer) * 0.1f,
            MeterAcCurrentB = GetUint16(ref registers,ref pointer) * 0.01f,
            MeterAcVoltageC = GetUint16(ref registers,ref pointer) * 0.1f,
            MeterAcCurrentC = GetUint16(ref registers,ref pointer) * 0.01f,
            MeterActivePowerA = GetInt32(ref registers,ref pointer) * 0.001f,
            MeterActivePowerB = GetInt32(ref registers,ref pointer) * 0.001f,
            MeterActivePowerC = GetInt32(ref registers,ref pointer) * 0.001f,
            MeterTotalActivePower = GetInt32(ref registers,ref pointer) * 0.001f,
            MeterReactivePowerA = GetInt32(ref registers,ref pointer),
            MeterReactivePowerB = GetInt32(ref registers,ref pointer),
            MeterReactivePowerC = GetInt32(ref registers,ref pointer),
            MeterTotalReactivePower = GetInt32(ref registers,ref pointer),
            MeterApparentPowerA = GetInt32(ref registers,ref pointer),
            MeterApparentPowerB = GetInt32(ref registers,ref pointer),
            MeterApparentPowerC = GetInt32(ref registers,ref pointer),
            MeterTotalApparentPower = GetInt32(ref registers,ref pointer),
            MeterPF = GetInt16(ref registers,ref pointer) * 0.01f,
            MeterGridFrequency = GetUint16(ref registers,ref pointer) * 0.01f,
            MeterTotalActiveEnergyFromGrid = GetUint32(ref registers,ref pointer) * 0.01f,
            MeterTotalActiveEnergyToGrid = GetUint32(ref registers,ref pointer) * 0.01f,
        };
        return meter;
    }


    public StorageSettings GetStorageSettings(byte slaveId)
    {
        int pointer = 0;
        ushort[] registers = ReadHoldingRegisters(slaveId, 43110, 43123);
        if (registers == null) return null;

        var storageSettings = new StorageSettings
        {
            EnergyStorageControlSwitch     = GetUint16(ref registers,ref pointer),
            BypassPowerEnable              = GetUint16(ref registers,ref pointer) > 0,
            BypassPowerSupplyRefVoltage    = GetUint16(ref registers,ref pointer) * 0.1f,
            BypassPowerSupplyFreq          = GetUint16(ref registers,ref pointer) * 0.01f,
            BatteryChargeDischargeEnable   = GetUint16(ref registers,ref pointer) > 0,
            BatteryModeSetting             = GetEnum<StorageSettings.Types.BatteryMode>(ref registers,ref pointer),
            BatteryChargeDischargeAmps     = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryChargeMaximumAmps       = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryDischargeMaximumAmps    = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryUnderVoltageProtection  = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryFloatVoltage            = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryChargeVoltage           = GetUint16(ref registers,ref pointer) * 0.1f,
            BatteryOvervoltageProtection   = GetUint16(ref registers,ref pointer) * 0.1f,
            OverloadBuckSetting            = GetUint16(ref registers,ref pointer) > 0,
        };
        return storageSettings;
    }



    public void SetDateTime(byte slaveId)
    {

        master.WriteMultipleRegisters(slaveId,43000,new ushort[]
        {
            (ushort)(DateTime.UtcNow.Year % 100),
            (ushort)DateTime.UtcNow.Month,
            (ushort)DateTime.UtcNow.Day,
            (ushort)DateTime.UtcNow.Hour,
            (ushort)DateTime.UtcNow.Minute,
            (ushort)DateTime.UtcNow.Second
        });
    }

    public void SetChargeDischarge(byte slaveId, TimeOnly ChargeFrom,TimeOnly ChargeTo, TimeOnly DischargeFrom,TimeOnly DischargeTo)
    {

        master.WriteMultipleRegisters(slaveId,43143,new ushort[]
        {
            (ushort)ChargeFrom.Hour,
            (ushort)ChargeFrom.Minute,
            (ushort)ChargeTo.Hour,
            (ushort)ChargeTo.Minute,
            (ushort)DischargeFrom.Hour,
            (ushort)DischargeFrom.Minute,
            (ushort)DischargeTo.Hour,
            (ushort)DischargeTo.Minute,
        });

        master.WriteMultipleRegisters(slaveId, 43110, new ushort[]
        {
            (ushort)35   //33 self use mode  //35 timed mode
        });
    }

    public void ForceCharge(byte slaveId)
    {

        master.WriteMultipleRegisters(slaveId, 43115, new ushort[]
        {
            (ushort)0   //0 charge //1 Discharge
        });


        master.WriteMultipleRegisters(slaveId, 43110, new ushort[]
        {
            (ushort)33   //33 self use mode  //35 timed mode
        });
    }

    public void SetChargeRate(byte slaveId, uint amps)
    {
        master.WriteMultipleRegisters(slaveId,43141,new ushort[]
        {
            (ushort)(amps / 0.1),
        });
    }

    public ushort GetChargeRate(byte slaveId)
    {
        return (ushort)(ReadHoldingRegister(slaveId,43141) * 0.1);
    }

    public void Reboot(byte slaveId)
    {
        master.WriteMultipleRegisters(slaveId,43007,new ushort[]
        {
            (ushort)0xBE,
        });
    }


    UInt16 GetUint16(ref ushort[] array, ref int pointer)
    {
        return (UInt16)(array[pointer++]);
    }
    UInt32 GetUint32(ref ushort[] array, ref int pointer)
    {
        var value = (UInt32)(array[pointer] << 16 | array[pointer+1]);
        pointer+=2;
        return value;
    }


    Int16 GetInt16(ref ushort[] array, ref int pointer)
    {
        return (Int16)(array[pointer++]);
    }
    Int32 GetInt32(ref ushort[] array, ref int pointer)
    {
        var value = (Int32)(array[pointer] << 16 | array[pointer+1]);
        pointer+=2;
        return value;
    }




    String GetString(ref ushort[] array, ref int pointer, int count)
    {
        var sb = new StringBuilder();
        int pointerTo = pointer + count;

        for (; pointer < pointerTo; pointer++)
        {
            sb.Append((char)(array[pointer] >> 8));
            sb.Append((char)(array[pointer] & 0xff));
        }

        return sb.ToString();
    }

    T GetEnum<T>(ref ushort[] array, ref int pointer)
    {
        if (System.Enum.IsDefined(typeof(T), (int)array[pointer++]))
        {
            return (T)(object)(int)array[0];
        }
        else
        {
            return (T)(object)0;
        }
    }

    public void Dispose()
    {
        port?.Dispose();
        tcpclient?.Dispose();
        master?.Dispose();
    }

    public void ResetComms()
    {
        Console.WriteLine("Solis Reset");
        Dispose();
        if (IPaddress != null)
        {
            OpenTCP();
        }
        else if (!String.IsNullOrEmpty(SerialDevice))
        {
            OpenSerial();
        }
    }
}
