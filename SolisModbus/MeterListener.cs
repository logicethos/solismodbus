using System.IO.Ports;
using SolisModbus.Proto;

namespace SolisModbus;

public class MeterListener : IDisposable
{
    private SerialPort _serialPort;
    string serialDevice;
    int baud;
    int slaveID;
    byte[] buffer = new byte[1000];
    bool ignoreDataCRC = true;
    private bool keepRunning = false;

    public delegate void NewReadingDelegate(object source, EventArgs args);
    public event NewReadingDelegate NewReading;

    MeterLive _meterLive = new MeterLive();

    public MeterListener(String serialDevice, int baud = 9600, int slaveID = 1)
    {
        this.serialDevice = serialDevice;
        this.baud = baud;
        this.slaveID = slaveID;
    }

    public MeterLive MeterLive
    {
        get
        {
            return _meterLive;
        }
    }

    public Task Start()
    {
        keepRunning = true;
        return Task.Run(() =>
            {
                Listen();
            }
        );
    }

    public void Stop()
    {
        keepRunning = false;
        _serialPort?.Dispose();
    }

    void Listen()
    {
        while (keepRunning)
        {
            int registerRequest=0;
            int registerCount=0;
            int bufEnd = 0;
            int sPointer = 0;
            const int functionNo = 4;

            try
            {
                Console.WriteLine($"Opening {serialDevice} {baud}");
                _serialPort = new SerialPort(serialDevice, baud);
                _serialPort.Open();
                _serialPort.ReadTimeout = 5000;
                bool waitSlaveRequest = true;


                int bytesRead;
                while ((bytesRead = _serialPort.Read(buffer, bufEnd, buffer.Length - bufEnd)) > 0)
                {
                    bufEnd += bytesRead;

                    while (sPointer < bufEnd - 8)
                    {
                        if (waitSlaveRequest)
                        {
                            //Looking for 01 04 0008 0001 B298
                            //            id fn reg1 count crc
                            if (bufEnd - sPointer + 1 >= 8)
                            {
                                for (; sPointer < bufEnd - 8; sPointer++) // potential frame start
                                {
                                    if (buffer[sPointer] != slaveID) continue;
                                    if (buffer[sPointer + 1] != functionNo) continue;

                                    var crc = ModRTU_CRC(sPointer, sPointer + 5);
                                    if ((crc & 0xff) == buffer[sPointer + 6] &&
                                        crc >> 8 == buffer[sPointer + 7]) //CRC match
                                    {
                                        waitSlaveRequest = false;
                                        registerRequest = (buffer[sPointer + 2] << 8) | buffer[sPointer + 3];
                                        registerCount = (buffer[sPointer + 4] << 8) | buffer[sPointer + 5];

                                        Console.WriteLine(
                                            $"Expecting: {registerRequest}  count: {registerCount} sPointer:{sPointer} bufEnd:{bufEnd}  bytesRead: {bytesRead}");
                                        //  Console.WriteLine(BitConverter.ToString(buffer,sPointer,bytesRead));
                                        sPointer += 8;
                                        break;
                                    }
                                    else
                                    {
                                        sPointer++;
                                        Console.WriteLine($"Failed Request CRC");
                                    }
                                }
                            }
                            else
                            {
                                break; //more data needed
                            }
                        }

                        if (!waitSlaveRequest)
                        {
                            //Looking for 01 04 02 DATA B298
                            //            id fn n DATA2n crc

                            for (; sPointer < bufEnd; sPointer++) // skip nulls
                            {
                                if (buffer[sPointer] != 0xff) break;
                            }

                            var expectedDATA = registerCount * 2;

                            if (bufEnd - sPointer + 1 >= expectedDATA + 5)
                            {

                                if (buffer[sPointer] == slaveID &&
                                    buffer[sPointer + 1] == functionNo &&
                                    buffer[sPointer + 2] == expectedDATA)
                                {
                                    var cPointer = sPointer + expectedDATA + 3;

                                    if (!ignoreDataCRC)
                                    {
                                        var crc = ModRTU_CRC(sPointer, cPointer - 1);
                                        if ((crc & 0xff) == buffer[cPointer] &&
                                            crc >> 8 == buffer[cPointer + 1]) //CRC match
                                        {
                                            ProcessRegisters(registerRequest, sPointer + 3, registerCount);
                                            Console.WriteLine($"got count: {registerCount}");
                                        }
                                        else
                                        {
                                            Console.WriteLine($"Failed Data CRC");
                                            var temp1 = new byte[cPointer - sPointer + 2];

                                            Array.Copy(buffer,sPointer,temp1,0,temp1.Length);
                                            Console.WriteLine(BitConverter.ToString(temp1));
                                        }
                                    }
                                    else
                                    {
                                        ProcessRegisters(registerRequest, sPointer + 3, registerCount);
                                    }

                                    waitSlaveRequest = true;
                                    sPointer = cPointer + 2;


                                }
                                else //Bad message reset buffer
                                {
                                    var e = sPointer + 20;
                                    if (e > bufEnd) e = bufEnd;
                                    Console.WriteLine($"sPointer={sPointer} bufEnd={bufEnd} read={bytesRead}");
                                    Console.WriteLine(BitConverter.ToString(buffer, sPointer, e - sPointer));

                                    sPointer = bufEnd;
                                    waitSlaveRequest = true;
                                    Console.WriteLine($"Failed Reply");

                                }
                            }
                            else
                            {
                                break; //More data needed
                            }
                        }
                    }

                    if (sPointer > bufEnd)
                    {
                        sPointer = 0;
                        bufEnd = 0;
                    }
                    else if (sPointer >= buffer.Length - 8) //Overrun buffer - shouldn't happen.
                    {
                        sPointer = 0;
                        bufEnd = 0;
                        Console.WriteLine($"Reset");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Meter: {serialDevice} {e.Message}");
                _serialPort?.Dispose();
            }
            finally
            {
                _serialPort?.Dispose();
                Console.WriteLine($"Meter: {serialDevice} Closed  {sPointer} {bufEnd}");
            }
            Thread.Sleep(5000); //Slow runnaway thread
        }
        _serialPort?.Dispose();
    }

    private void ProcessRegisters(int registerRequest, int startIndex, int registerCount)
    {
        bool sanity = true;
        if (registerRequest == 00000)
        {
            _meterLive.VoltL1 = GetRegister(startIndex + 00000);
            _meterLive.VoltL2 = GetRegister(startIndex + 00004);
            _meterLive.VoltL3 = GetRegister(startIndex + 00008);

            _meterLive.AmpsL1 = GetRegister(startIndex + 00012);
            _meterLive.AmpsL2 = GetRegister(startIndex + 00016);
            _meterLive.AmpsL3 = GetRegister(startIndex + 00020);

            _meterLive.PowerL1 = GetRegister(startIndex + 00024);
            _meterLive.PowerL2 = GetRegister(startIndex + 00028);
            _meterLive.PowerL3 = GetRegister(startIndex + 00032);

            var freq = GetRegister(startIndex + (00071-1)*2);


            if (_meterLive.Frequency < 49 || _meterLive.Frequency > 51)
            {

                //Console.WriteLine($"Sanaty Check: Frequency  {_meterLive.Frequency}");
                //sanity = false;
            }
            else
            {
                _meterLive.Frequency = freq;
            }

            var voltTotal = _meterLive.VoltL1 + _meterLive.VoltL2 + _meterLive.VoltL3;
            if ( voltTotal < (3 * 220) || voltTotal > (3 * 250))
            {
                Console.WriteLine("Sanaty Check: Volts");
                sanity = false;
            }

            if (sanity) NewReading(this, EventArgs.Empty);
        }
    }

    Single GetRegister(int startIndex)
    {
        Array.Reverse(buffer,startIndex,4);
        return BitConverter.ToSingle(buffer, startIndex);
    }


    //Calculate CRC between two buffer pointers
    public ushort ModRTU_CRC(int start, int end)
    {
        UInt16 crc = 0xFFFF;

        for (int pos = start; pos <= end; pos++)
        {
            crc ^= (UInt16)buffer[pos]; // XOR byte into least sig. byte of crc

            for (int i = 8; i != 0; i--) // Loop over each bit
            {
                if ((crc & 0x0001) != 0) // If the LSB is set
                {
                    crc >>= 1; // Shift right and XOR 0xA001
                    crc ^= 0xA001;
                }
                else // Else LSB is not set
                    crc >>= 1; // Just shift right
            }
        }

        // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
        return crc;
    }

    public void Dispose()
    {
        Stop();
    }

}