FROM mcr.microsoft.com/dotnet/sdk:7.0-alpine3.16 AS builder

ENV PROTOBUF_PROTOC=/usr/bin/protoc
ENV gRPC_PluginFullPath=/usr/bin/grpc_csharp_plugin

RUN apk add --no-cache git protobuf protobuf-dev grpc

COPY . /SolisModbus
WORKDIR /SolisModbus

RUN mkdir -p /app
RUN dotnet publish SolisModbus.sln --configuration Release \
           -p:GitHash=$(git rev-parse HEAD) \
           -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) \
           -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) \
           -p:GitTag=$(git describe --tags | sed 's/-g.*//') \
           -p:PublishDir=/app \
           -r linux-arm -p:PublishSingleFile=true --self-contained true

RUN dotnet publish SolisModbus.sln --configuration Release \
           -p:GitHash=$(git rev-parse HEAD) \
           -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) \
           -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) \
           -p:GitTag=$(git describe --tags | sed 's/-g.*//') \
           -p:PublishDir=/app \
           -r linux-arm64 -p:PublishSingleFile=true --self-contained true


ENTRYPOINT ["/bin/bash"]
