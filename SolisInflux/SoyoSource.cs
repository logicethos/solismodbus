using System.IO.Ports;

namespace SolisInflux;



public class SoyoSource : IDisposable
{

    SerialPort port;
    uint Watts;
    Task? SendTask = null;
    CancellationTokenSource? cts = null;
    //uint WattsMax = 900;


    public SoyoSource(string comPort, int baud = 4800)
    {
        OpenSerial(comPort, baud);
    }


    void OpenSerial(string comPort, int baud)
    {
        // Set up the serial port configuration.
        port = new SerialPort()
        {
            PortName = comPort,
            BaudRate = baud,
            DataBits = 8,
            Parity = Parity.None,
            StopBits = StopBits.One,
            //    ReadTimeout = 1000
        };

        Console.WriteLine($"Opening port {port.PortName} Baud:{port.BaudRate}, Timeout {port.ReadTimeout}");
        port.Open();


    }

    public void Start(uint power)
    {
        if (power > Program.SoyoMaxWatts) power = Program.SoyoMaxWatts;

        if (Watts != power)
        {
            Watts = power;
            Console.WriteLine($"SoysSource: Power={Watts}");
        }


        if (SendTask != null && !SendTask.IsCanceled) return;

        cts = new CancellationTokenSource();

        SendTask = Task.Run(async () =>
        {
            Console.WriteLine("SoysSource: Start");
            while (!cts.Token.IsCancellationRequested)
            {
                try
                {
                    SendPower(Watts);
                    await Task.Delay(500, cts.Token);
                }
                catch (TaskCanceledException)
                {
                }
                catch (Exception ex)
                {

                }
            }
        });
    }

    public void Stop()
    {
    //    if (cts != null && !cts.IsCancellationRequested)
     //   {
           // Console.WriteLine("SoysSource: Stop");
           // cts.Cancel();

        //}
        Watts = 0;
    }

    public void SendPower(uint power)
    {

        var frame = new byte[8];


        frame[0] = 0x24;
        frame[1] = 0x56;
        frame[2] = 0x00;
        frame[3] = 0x21;
        frame[4] = (byte)(power >> 8);
        frame[5] = (byte)(power & 0xff);
        frame[6] = 0x80;
        frame[7] = (byte)(264 - frame[4] - frame[5]); // Checksum

        port.Write(frame, 0, 8);
    }

    public void Dispose()
    {
        port?.Dispose();
    }
}
