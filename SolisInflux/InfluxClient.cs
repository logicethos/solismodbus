using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Core.Flux.Domain;
using InfluxDB.Client.Writes;
using SolisInflux.Proto;

namespace SolisInflux;



class InfluxClient : IDisposable
{

    static EnergyLive  EnergyLiveAverage = new SolisInflux.Proto.EnergyLive();
    InfluxDBClient? influx;
    int averageCount = 0;
    readonly object averageLock = new object();
    string _influxUrl;
    string APIToken;
    WriteApi _writeApi;
    string Organisation;
    int LastWriteMinute = -1;

    public InfluxClient(string InfluxURL,string organisation, string apiToken)
    {
        _influxUrl = InfluxURL;
        APIToken = apiToken;
        Organisation = organisation;
    }

    public Task Start(CancellationToken cancellationToken)
    {
        Open();
        return Task.Run(() =>
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                Thread.Sleep(10000);
                Write();
            }

        },cancellationToken);
    }

    public void Open()
    {
        influx?.Dispose();
        influx = InfluxDBClientFactory.Create(_influxUrl, APIToken);
        _writeApi = influx.GetWriteApi();
    }

    public async Task UpdateValues(EnergyLive energy)
    {
        lock (averageLock)
        {
            averageCount++;
            EnergyLiveAverage.GridAmps += energy.GridAmps;
            EnergyLiveAverage.GarageAmps += energy.GarageAmps;
            EnergyLiveAverage.SolarFitAmps += energy.SolarFitAmps;
            EnergyLiveAverage.GridWatts += energy.GridWatts;
            EnergyLiveAverage.GarageWatts += energy.GarageWatts;
            EnergyLiveAverage.SolarFitWatts += energy.SolarFitWatts;
            EnergyLiveAverage.GridVoltage += energy.GridVoltage;
            EnergyLiveAverage.Frequency += energy.Frequency;

            EnergyLiveAverage.SolarGenerationWatts += energy.SolarGenerationWatts;
            EnergyLiveAverage.BatteryVoltage += energy.BatteryVoltage;
            EnergyLiveAverage.BatteryAmps += energy.BatteryAmps;

            EnergyLiveAverage.BatteryCapacitySOC = energy.BatteryCapacitySOC;

            EnergyLiveAverage.EnergyNetWatts += energy.EnergyNetWatts;

        }
    }

    public async Task UpdateTotals(EnergyTotals energy)
    {
        PointData point;
        point = PointData.Measurement("totals")
            .Field("TotalEnergyGenerationKw", energy.TotalEnergyGeneration)
            .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

        _writeApi.WritePoint(point,"energy",Organisation);

    }

    public void Write()
    {
        if (averageCount==0) return;

        PointData point;
        lock (averageLock)
        {
            EnergyLiveAverage.GridAmps /= averageCount;
            EnergyLiveAverage.GarageAmps /= averageCount;
            EnergyLiveAverage.SolarFitAmps /= averageCount;
            EnergyLiveAverage.GridWatts /= averageCount;
            EnergyLiveAverage.GarageWatts /= averageCount;
            EnergyLiveAverage.SolarFitWatts /= averageCount;
            EnergyLiveAverage.GridVoltage /= averageCount;
            EnergyLiveAverage.Frequency /= averageCount;

            EnergyLiveAverage.SolarGenerationWatts /= averageCount;
            EnergyLiveAverage.BatteryVoltage /= averageCount;
            EnergyLiveAverage.BatteryAmps /= averageCount;

            EnergyLiveAverage.EnergyNetWatts /= averageCount;

          //  EnergyLiveAverage.BatteryCapacitySOC = EnergyLiveAverage.BatteryCapacitySOC;


            point = PointData.Measurement("meter")
                .Field("GridV", Math.Round(EnergyLiveAverage.GridVoltage,3))
                .Field("GridA", Math.Round(EnergyLiveAverage.GridAmps,3))
                .Field("GarageA", Math.Round(EnergyLiveAverage.GarageAmps,3))
                .Field("SolarA", Math.Round(EnergyLiveAverage.SolarFitAmps,3))
                .Field("GridW", Math.Round(EnergyLiveAverage.GridWatts,3))
                .Field("GarageW", Math.Round(EnergyLiveAverage.GarageWatts,3))
                .Field("SolarFitW", Math.Round(EnergyLiveAverage.SolarFitWatts,3))
                .Field("EnergyNetW", Math.Round(EnergyLiveAverage.EnergyNetWatts,3))
                .Timestamp(DateTime.UtcNow, WritePrecision.Ns);
            _writeApi.WritePoint(point,"energy",Organisation);

            if (LastWriteMinute != DateTime.UtcNow.Minute)
            {
                point = PointData.Measurement("meter").Field("GridHz", EnergyLiveAverage.Frequency).Timestamp(DateTime.UtcNow, WritePrecision.Ns);
                _writeApi.WritePoint(point,"energy",Organisation);
            }



            point = PointData.Measurement("battery")
                .Field("Volts", Math.Round(EnergyLiveAverage.BatteryVoltage,3))
                .Field("Amps", Math.Round(EnergyLiveAverage.BatteryAmps,3))
                .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

            _writeApi.WritePoint(point,"energy",Organisation);

            if (LastWriteMinute != DateTime.UtcNow.Minute)
            {
                point = PointData.Measurement("battery").Field("SOC", Math.Round(EnergyLiveAverage.BatteryCapacitySOC, 3)).Timestamp(DateTime.UtcNow, WritePrecision.Ns);
                _writeApi.WritePoint(point,"energy",Organisation);
            }


            point = PointData.Measurement("solar")
                .Field("RoofW", Math.Round(EnergyLiveAverage.SolarGenerationWatts,3))
                .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

            _writeApi.WritePoint(point,"energy",Organisation);


            EnergyLiveAverage.GridAmps = 0;
            EnergyLiveAverage.GarageAmps = 0;
            EnergyLiveAverage.SolarFitAmps = 0;
            EnergyLiveAverage.GridWatts = 0;
            EnergyLiveAverage.GarageWatts = 0;
            EnergyLiveAverage.SolarFitWatts = 0;
            EnergyLiveAverage.GridVoltage = 0;
            EnergyLiveAverage.Frequency = 0;

            EnergyLiveAverage.SolarGenerationWatts = 0;
            EnergyLiveAverage.BatteryVoltage = 0;
            EnergyLiveAverage.BatteryAmps = 0;

            EnergyLiveAverage.EnergyNetWatts = 0;

            averageCount = 0;

        }

        if (LastWriteMinute != DateTime.UtcNow.Minute)
        {
            LastWriteMinute = DateTime.UtcNow.Minute;
        }
        _writeApi.Flush();
    }

    public void Dispose()
    {
        influx?.Dispose();
        _writeApi?.Dispose();
    }
}