using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using ConsoleServer;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using ProtoHelper;
//using Docker.DotNet.Models;
//using ConsoleServerDocker;
using ILogger = Serilog.ILogger;

namespace SolisInflux.TelnetConsole;

public partial class clsTelnetAppSession
{
    public  TelnetSession TelnetSession { get; init; }
    public AnsiTelnetConsole AnsiConsole => TelnetSession.AnsiConsole;
    private ILogger _logger;

    public clsTelnetAppSession(TelnetSession telnetSession , ILogger logger)
    {
        TelnetSession = telnetSession;
        _logger = logger;
    }

    public void Run()
    {
        try
        {

            bool result;
            do
            {
                //  var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;

                var menulist = new List<SelectionFunction<bool>>();


                menulist.Add(new SelectionFunction<bool>($"Grid display", () =>
                {


                    var table = new Table().LeftAligned();

                    AnsiConsole.Live(table)
                        .Start(ctx =>
                        {
                            table.AddColumn("Mesurement");
                            table.AddColumn("Watts");

                            table.AddRow("Grid", "0");
                            table.AddRow("Net", "0");
                            table.AddRow("Solar", "0");
                            table.AddRow("Battery", "0");
                            table.AddRow("Fit", "0");

                            ctx.Refresh();

                            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

                        using (var run = Task.Run(() =>
                        {
                            try
                            {
                                while (!cancellationTokenSource.IsCancellationRequested)
                                {
                                    table.Rows.Update(0, 1, new Text($"{Program.EnergyLive.GridWatts}"));
                                    table.Rows.Update(1, 1, new Text($"{Program.EnergyLive.EnergyNetWatts}"));
                                    table.Rows.Update(2, 1, new Text($"{Program.EnergyLive.SolarGenerationWatts}"));
                                    table.Rows.Update(3, 1, new Text($"{Program.EnergyLive.BatteryAmps * Program.EnergyLive.BatteryVoltage}"));
                                    table.Rows.Update(4, 1, new Text($"{Program.EnergyLive.SolarFitAmps}"));

                                    ctx.Refresh();
                                    Task.Delay(3000, cancellationTokenSource.Token).Wait();
                                }
                            }catch(Exception)
                            {

                            }
                        },cancellationTokenSource.Token))
                        {
                            AnsiConsole.Input.ReadKey(false);
                            cancellationTokenSource.Cancel();
                            Task.Delay(500).Wait();
                        }
                });


                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"Get Solis Storage Settings", () =>
                {
                    AnsiConsole.Write(Program.SolisCom.GetStorageSettings(1).ToProtoTree());
                    End();
                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"Get Solis Holding Register", () =>
                {
                    var addressText = AnsiConsole.Ask<string>("[green]Solis address[/]?");
                    ushort address;
                    if (ushort.TryParse(addressText, out address))
                    {
                        var value = Program.SolisCom.ReadHoldingRegister(1,address);
                        AnsiConsole.WriteLine($"{value} (0x{value:X})");
                    }
                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Get Solis Input Register", () =>
                {
                    var addressText = AnsiConsole.Ask<string>("[green]Solis address[/]?");
                    ushort address;
                    if (ushort.TryParse(addressText, out address))
                    {
                        var value = Program.SolisCom.ReadInputRegister(1,address);
                        AnsiConsole.WriteLine($"{value} (0x{value:X})");
                    }
                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Write Solis Register", () =>
                {
                    var addressText = AnsiConsole.Ask<string>("[red]Solis address[/]?");
                    ushort address;
                    if (ushort.TryParse(addressText, out address))
                    {
                        var value = Program.SolisCom.ReadInputRegister(1, address);
                        AnsiConsole.WriteLine(value.ToString());


                        var regValueText = AnsiConsole.Ask<string>("[red]Value[/]?");
                        ushort regValue;
                        if (ushort.TryParse(regValueText, out regValue))
                        {
                            Program.SolisCom.WriteRegister(1, address, regValue);
                        }
                    }

                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Set Default charge times", () =>
                {
                    Program.SolisCom.SetChargeDischarge(1, new TimeOnly(23,30),
                        new TimeOnly(05,30),
                        new TimeOnly(05,30),
                        new TimeOnly(23,30));

                    Program.startFreeTime = null;
                    Program.endFreeTime = null;

                    End();
                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"Set Free Electricity 1-2 BST", () =>
                {
                    Program.SolisCom.SetChargeDischarge(1, new TimeOnly(12,00),
                        new TimeOnly(13,00),
                        new TimeOnly(13,00),
                        new TimeOnly(23,30));

                    Program.SolisCom.SetChargeRate(1, 70);

                    Program.startFreeTime = new TimeSpan(12, 00, 00);
                    Program.endFreeTime = new TimeSpan(13, 00, 00);

                    End();
                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"Charge now!", () =>
                {

                    //Program.SolisCom.ForceCharge(1);
                    Program.SolisCom.SetChargeDischarge(1, new TimeOnly(0,0),
                        new TimeOnly(23,59),
                        new TimeOnly(23,59),
                        new TimeOnly(23,59));
                    End();
                    return true;
                }));

                menulist.Add(new SelectionFunction<bool>($"Set Charge Amps", () =>
                {

                    AnsiConsole.WriteLine($"Charge Rate (max 70A): {Program.SolisCom.GetChargeRate(1)}");

                    var valueText = AnsiConsole.Ask<string>("[green]Enter charge Amps[/]?");
                    uint amps;
                    if (uint.TryParse(valueText, out amps) && amps > 0)
                    {
                        Program.SolisCom.SetChargeRate(1,amps);
                    }

                    End();
                    return true;
                }));


                menulist.Add(new SelectionFunction<bool>($"Set Soyo Inverter Max Rate", () =>
                {

                    AnsiConsole.WriteLine($"Max Rate (max 900): {Program.SoyoMaxWatts}");

                    var valueText = AnsiConsole.Ask<string>("[green]Enter Watts[/]?");
                    uint watts;
                    if (uint.TryParse(valueText, out watts) && watts < 900)
                    {
                        Program.SoyoMaxWatts = watts;
                    }

                    End();
                    return true;
                }));



                menulist.Add(new SelectionFunction<bool>("Exit", () => { return false; }));

                ShowScreenHeader();

                var option = AnsiConsole.Prompt(
                    new SelectionPrompt<SelectionFunction<bool>>()
                        .Title("[magenta]Main Menu[/]")
                        .PageSize(20)
                        .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                        .AddChoices(menulist));

                result = option.MenuAction();

            } while (result);

        }
        catch (Exception e)
        {
            AnsiConsole.WriteException(e);
        }

    }

    public async Task ShowScreenHeader()
    {

        AnsiConsole.Clear(true);

        var rule = new Rule("[white]ACSharp Console[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.WriteLine();
        var grid = new Grid();
        grid.AddColumn();
        grid.AddColumn();
        grid.AddColumn();

          //  grid.AddRow("HW 0: ", $"[green]{Program.PiIO?.MsOn0}[/]", $"[green]{Program.PiIO?.MsOff0}[/]");
          //  grid.AddRow("HW 1: ", $"[green]{Program.PiIO?.MsOn1}[/]", $"[green]{Program.PiIO?.MsOff1}[/]");


//            grid.AddRow("HW 0: ", $"[green]{Program.PiIO?.PWM0Frequency}[/]hz", $"[green]{Program.PiIO?.PWM0DutyCycle:P0}[/]");
 //           grid.AddRow("HW 1: ", $"[green]{Program.PiIO?.PWM1Frequency}[/]hz", $"[green]{Program.PiIO?.PWM1DutyCycle:P0}[/]");
          //  grid.AddRow("Timer", $"{Program.HotWaterTimer}", $"[red]{(Program.DisableHotWaterTimer ? "Disabled" : "")}[/]");



        AnsiConsole.Write(grid);
       // var rule2 = new Rule( $"[blue]{AssemblyGitCommitTag.GetValueOrTime()}[/]");
       var rule2 = new Rule( $"[blue]Solis[/]");
        rule2.RuleStyle("blue");
        rule2.RightAligned();
        AnsiConsole.Write(rule2);
    }

    public string MenuActiveMarkup(string input, bool active)
    {
        if (active) return input;
        return $"[grey]{input}[/]";
    }

    public bool AreYouSure(string prompt = "Confirm")
    {
        var rule = new Rule($"[white]{prompt}[/]");
        rule.RuleStyle("red");
        rule.LeftAligned();
        AnsiConsole.Write(rule);
        
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));
        
        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title("Are you sure")
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

    public bool YesNo(string prompt = "Select")
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }


    public void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }
}
