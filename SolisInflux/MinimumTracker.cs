namespace SolisInflux;

public class MinimumTracker<T> where T : IComparable<T>
{
    private readonly Queue<(DateTime, T)> _values;
    private readonly int _windowSizeInSeconds;

    public MinimumTracker(int windowSizeInSeconds = 180)
    {
        _values = new Queue<(DateTime, T)>();
        _windowSizeInSeconds = windowSizeInSeconds;
    }

    public void AddValue(T value)
    {
        DateTime now = DateTime.UtcNow;

        // Remove values outside the 3-minute window
        while (_values.Count > 0 && (now - _values.Peek().Item1).TotalSeconds > _windowSizeInSeconds)
        {
            _values.Dequeue();
        }

        // Add the new value with the current time
        _values.Enqueue((now, value));
    }

    public T GetMinValue()
    {
        // Ensure there are values in the queue
        if (_values.Count == 0)
        {
            return default(T);
        }

        // Find the minimum value in the current window
        T minValue = _values.Peek().Item2;
        foreach (var (timestamp, value) in _values)
        {
            if (value.CompareTo(minValue) < 0)
            {
                minValue = value;
            }
        }

        return minValue;
    }

    public T GetMedianValue()
    {
        // Ensure there are values in the queue
        if (_values.Count == 0)
        {
            return default(T);
        }

        // Extract the values and sort them
        var sortedValues = _values.Select(v => v.Item2).OrderBy(v => v).ToList();
        int count = sortedValues.Count;

        if (count % 2 == 1)
        {
            // If odd, return the middle value
            return sortedValues[count / 2];
        }
        else
        {
            // If even, return the average of the two middle values
            dynamic a = sortedValues[(count / 2) - 1];
            dynamic b = sortedValues[count / 2];
            return (T)((a + b) / 2);
        }
    }

    public T GetBottomPercentAverage(int percent)
    {
        // Ensure there are values in the queue
        if (_values.Count == 0)
        {
            return default(T);
        }

        // Extract the values and sort them
        var sortedValues = _values.Select(v => v.Item2).OrderBy(v => v).ToList();
        int count = sortedValues.Count;

        // Calculate the number of elements in the bottom %
        int bottomCount = (int)Math.Ceiling(count * (percent / 100f));

        // Take the bottom 10% values
        var bottomValues = sortedValues.Take(bottomCount);

        // Calculate the average of these values
        dynamic sum = default(T);
        foreach (var value in bottomValues)
        {
            sum += value;
        }

        return (T)(sum / bottomCount);
    }

    public void Reset()
    {
        _values.Clear();
    }
}