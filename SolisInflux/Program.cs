﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using Google.Protobuf;
using SolisInflux;
using SolisInflux.Proto;
using SolisModbus;
using System.Linq;
using ConsoleServer;
using Serilog;
using SolisInflux.TelnetConsole;

namespace SolisInflux;


public static class Program
{
    static string SerialPortMeter;
    static string SerialPortMeterBaud;
    static string SerialPortSolis;
    static string SerialPortSolisBaud;

    static public EnergyLive  EnergyLive = new SolisInflux.Proto.EnergyLive();
    static EnergyTotals  EnergyTotals = new SolisInflux.Proto.EnergyTotals();

    enum Mode
    {
        Normal,
        ChargeMax
    };

    static List<(uint SOC, uint AMPS)> socAmpsList = new List<(uint SOC, uint AMPS)>
            {
                (30, 70),
                (40, 60),
                (60, 45),
                (70, 30),
            };
    
    

    static public ModbusClient SolisCom;
    static MeterListener Meter;
    static InfluxClient Influx;
    static SoyoSource SoyoSourceInv;
    static UdpClient udpClient = new UdpClient();

    static readonly object valueLock = new object();


    static byte slaveId;
    static CancellationToken cancellationToken = new CancellationToken();

    static IPAddress broardcastIPAddress = IPAddress.Parse("192.168.0.255");
    static int broardcastIPPort = 1234;

    static TimeSpan startCheapRate = new TimeSpan(23, 30, 0);
    static TimeSpan endCheapRate = new TimeSpan(5, 30, 0);

    static public TimeSpan? startFreeTime = null;
    static public TimeSpan? endFreeTime = null;

    static int LastHour = -1;
    static int LastDay = -1;

    static TelnetServer telnetServer;
    public static uint SoyoMaxWatts { get; set; } = 900;

    static async Task Main()
    {
        slaveId = 1;

#if !DEBUG

        // Read the environment variables
        var env_solisCom = Environment.GetEnvironmentVariable("SOLISCOM")?.Split(",", StringSplitOptions.TrimEntries);
        if (env_solisCom == null || env_solisCom.Length != 2) {Console.WriteLine($"Incorrect SOLISCOM ({env_solisCom?.Length})"); return;}

        var env_meterCom = Environment.GetEnvironmentVariable("METERCOM")?.Split(",", StringSplitOptions.TrimEntries);
        if (env_meterCom == null || env_meterCom.Length != 2) {Console.WriteLine("Incorrect METERCOM"); return;}

        var env_soyoSourceCom = Environment.GetEnvironmentVariable("SOYOCOM")?.Split(",", StringSplitOptions.TrimEntries);
        if (env_soyoSourceCom == null || env_soyoSourceCom.Length != 2) {Console.WriteLine("Incorrect SOYOCOM"); return;}

        var env_influxDB = Environment.GetEnvironmentVariable("INFLUXDB")?.Split(",", StringSplitOptions.TrimEntries);
        if (env_influxDB == null || env_influxDB.Length != 3) {Console.WriteLine("Incorrect INFLUXDB"); return;}
#else

        String[] env_solisCom = new[] { "/tmp/virtualSerialPort0", "38400" };
        String[] env_meterCom = new[] { "/tmp/virtualSerialPort1", "9600" };
        String[] env_soyoSourceCom = new[] { "/tmp/virtualSerialPort3", "4800" };
        String[] env_influxDB = new[] { "http://192.168.0.10:8086", "Home","8-W1dldHARa4Dr-3PrYsdkYPh0hS9QTaNn2aT1pE3f1c4bs_Ui1OJJqcchKbmXPpb3v5WDGko2D6Igq2rToHdA==" };

#endif

        try
        {
            broardcastIPAddress = IPAddress.Parse("192.168.0.255");
            SolisCom = new ModbusClient(env_solisCom[0], int.Parse(env_solisCom[1]));
            Meter = new MeterListener(env_meterCom[0], int.Parse(env_meterCom[1]));
            SoyoSourceInv = new SoyoSource(env_soyoSourceCom[0], int.Parse(env_soyoSourceCom[1]));

            Influx = new InfluxClient(env_influxDB[0], env_influxDB[1], env_influxDB[2]);
            udpClient.EnableBroadcast = true;

            var meterTsk = Meter.Start();
            Meter.NewReading += (source, args) =>
            {
                lock (valueLock)
                {
                    EnergyLive.GridAmps = Meter.MeterLive.AmpsL1.SetBounds(200);
                    EnergyLive.GarageAmps = Meter.MeterLive.AmpsL2.SetBounds(200);
                    EnergyLive.SolarFitAmps = Meter.MeterLive.AmpsL3.SetBounds(200);
                    EnergyLive.GridWatts = Meter.MeterLive.PowerL1.SetBounds(50000, 0);
                    EnergyLive.GarageWatts = Meter.MeterLive.PowerL2.SetBounds(50000, 0);
                    EnergyLive.SolarFitWatts = Meter.MeterLive.PowerL3.SetBounds(10000, 0);
                    EnergyLive.GridVoltage = Meter.MeterLive.VoltL1.SetBounds(1000, 1);
                    EnergyLive.Frequency = Meter.MeterLive.Frequency.SetBounds(70, 5);

                    //TotalSupplyWatts=GridWatts+SolarGenerationWatts

                    //EnergyConsumedWatts=TotalSupplyWatts+BatteryWatts
                    //NetEnergyToHome=TotalSupplyWatts−∣BatteryWatts∣

                    EnergyLive.EnergyNetWatts = EnergyLive.GridWatts + EnergyLive.SolarGenerationWatts +
                                                (EnergyLive.BatteryAmps * EnergyLive.BatteryVoltage);

                    SendUDP();
                    Influx.UpdateValues(EnergyLive);

                }
            };

            int SoyValue = 0;
            uint belowZeroCount = 0;
            DateTime SoySetNext = DateTime.UtcNow.AddSeconds(60);
            ushort regpointer0 = 0;
            var file = File.Create("addresses");


            var minTracker = new MinimumTracker<int>(3 * 60);


            telnetServer = new ConsoleServer.TelnetServer(3333); //(int)Settings.ConsolePort
            //  Log.Information("Starting Telnet Server");
            telnetServer.StartListen(new Action<TelnetSession>((session) =>
            {
                var telnetAppSession = new clsTelnetAppSession(session, Log.Logger);
                telnetAppSession.Run();
            }));


            var influxTask = Influx.Start(cancellationToken);
            int errorCount = 0;
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var gotSolisData = await GetSolisData(cancellationToken);
                    if (!gotSolisData)
                    {
                        errorCount++;
                        Console.WriteLine($"Solis communication Error {errorCount}");
                        if (errorCount >= 2)
                        {
                            SolisCom.ResetComms();
                            errorCount = 0;
                        }
                    }

                    await Task.Delay(1000, cancellationToken);


                    if (LastHour != DateTime.UtcNow.Hour)  //New hour
                    {
                        LastHour = DateTime.UtcNow.Hour;
                        if (LastDay != DateTime.UtcNow.Day)  //New Day
                        {
                            LastDay = DateTime.UtcNow.Day;
                            SolisCom.SetDateTime(slaveId);
                        }

                        if (LastHour == 23 || LastHour == 0)
                        {
                            SolisCom.SetChargeRate(slaveId, GetAmps((uint)EnergyLive.BatteryCapacitySOC));
                        }
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await Task.Delay(10000, cancellationToken);
                }

                minTracker.AddValue((int)EnergyLive.EnergyNetWatts);
                if (EnergyLive.EnergyNetWatts < 0) belowZeroCount++;


                Console.WriteLine($"Net = {EnergyLive.EnergyNetWatts:0}");

                if (IsCheapRate() || IsFreeTime() || SoyoMaxWatts == 0)
                {
                    SoyoSourceInv.Stop();
                }
                else
                {

                    if (belowZeroCount > 3)
                    {
                        SoyoSourceInv.Start(0);
                        belowZeroCount = 0;
                    }
                    else if (DateTime.UtcNow >= SoySetNext)
                    {
                        // SoyValue = (int)(minTracker.GetMinValue() * .85);
                        SoyValue = (int)(minTracker.GetBottomPercentAverage(50) * .85);
                        SoyoSourceInv.Start(SoyValue > 0 ? (uint)SoyValue : 0);
                        SoySetNext = DateTime.UtcNow.AddSeconds(30);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            telnetServer.Dispose();
            Log.CloseAndFlush();
        }

    }

    static string FormatData(int i, ushort[] dat)
    {
        // Convert integer i to string
        string formattedString = i.ToString();

        // Append each element of the ushort array to the string, separated by spaces
        foreach (ushort value in dat)
        {
            formattedString += " " + value;
        }

        return formattedString;
    }

    static void SendUDP()
    {
        byte[] energyBytes;
        using (MemoryStream stream = new MemoryStream())
        {
            CodedOutputStream output = new CodedOutputStream(stream);
            EnergyLive.WriteTo(output);
            output.Flush();
            energyBytes = stream.ToArray();
        }

        udpClient.Send(energyBytes, energyBytes.Length, new IPEndPoint(broardcastIPAddress, broardcastIPPort));
    }


    static bool IsCheapRate()
    {
        TimeSpan currentTimeOfDay = DateTime.UtcNow.TimeOfDay;

        if (startCheapRate < endCheapRate)
        {
            // Case 1: Time range does not span midnight
            return currentTimeOfDay >= startCheapRate && currentTimeOfDay < endCheapRate;
        }
        else
        {
            // Case 2: Time range spans midnight
            return currentTimeOfDay >= startCheapRate || currentTimeOfDay < endCheapRate;
        }
    }

    static bool IsFreeTime()
    {
        if (!startFreeTime.HasValue || !endFreeTime.HasValue) return false;

        TimeSpan currentTimeOfDay = DateTime.UtcNow.TimeOfDay;

        if (startFreeTime < endFreeTime)
        {
            // Case 1: Time range does not span midnight
            return currentTimeOfDay >= startFreeTime && currentTimeOfDay < endFreeTime;
        }
        else
        {
            // Case 2: Time range spans midnight
            return currentTimeOfDay >= startFreeTime || currentTimeOfDay < endFreeTime;
        }
    }


    static async Task<bool> GetSolisData (CancellationToken cancellationToken)
    {
            try
            {
                var battery = SolisCom.GetBattery(slaveId);
                var generation = SolisCom.GetTotalGeneration(slaveId);
                var dcPower = SolisCom.GetDcPower(slaveId);

                if (generation != null)
                {
                    if (EnergyTotals.TotalEnergyGeneration != generation.TotalEnergyGeneration)
                    {
                        EnergyTotals.TotalEnergyGeneration = generation.TotalEnergyGeneration;
                        Influx.UpdateTotals(EnergyTotals);
                    }
                }

                lock (valueLock)
                {
                    if (battery != null)
                    {
                        EnergyLive.BatteryAmps = battery.BatteryCurrentBMS;
                        EnergyLive.BatteryVoltage = battery.BatteryVoltageBMS;
                        EnergyLive.BatteryCapacitySOC = battery.BatteryCapacitySOC;
                    }

                    if (dcPower != null && dcPower.TotalOutputPower < 100000)  //Set max, as we have seen bad data
                    {
                        EnergyLive.SolarGenerationWatts = dcPower.TotalOutputPower;
                    }
                }

                return battery != null || generation != null || dcPower != null;  //Return true if we got at least one value.
            }
            catch (Exception ex)
            {
                Console.WriteLine($"SolisCom: {ex.Message}");
            }

            return false;
    }

    static uint GetAmps(uint soc)
        {
            // Find the closest SOC value that is less than or equal to the given SOC
            (uint SOC, uint AMPS) closest = socAmpsList[0];
            foreach (var item in socAmpsList)
            {
                if (item.SOC <= soc)
                {
                    closest = item;
                }
                else
                {
                    break;
                }
            }
            return closest.AMPS;
        }

    static float SetBounds(this float value, int maxValue, int decimalPoints = 3)
    {
        if (value > maxValue || value < -maxValue)
        {
            Console.WriteLine($"Max Value = {value} > {maxValue}");
            return 0;
        }

        return (float)Math.Round(value, decimalPoints);
    }

}